---
longtitle: "Content from redmine_taskjuggler on GitHub.com"
menutitle: "RedTask"
title: "Atlassian Redtask Wiki" 
author: Chris Mann <chris@caplancity.com>
weight: 20
date: 2015
draft: false
---

This content comes from [redtask CAP](https://redtask.atlassian.net/wiki/spaces/CAP) Confluence Wiki on Atlassian. Atlassian has generously allocated resources to our efforts. There is a good amount of content here.

{{% children depth="2" %}}
