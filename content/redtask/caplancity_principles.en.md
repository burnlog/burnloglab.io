---
title: "Principles"
source: "https://redtask.atlassian.net/wiki/spaces/CAP/pages/2293779/Caplancity+Principles"
date: 2015-04-07
author: "Chris Mann <chris@mann.fr>"
weight: 60
---

The goal here is to examine ways and tools that can create credible and proven roadmaps. The process under examination is how to transition from budget to roadmap and back again. In order to get there, we need to admit a couple of simple common-sense principles.

First of all, budgets and roadmaps are related. This is probably the reasoning behind the term "ways and means." This means that the budget and the roadmap follow the same trajectory, and are part of the same process.

Second of all, budgets and roadmaps exist in an time-agnostic indivisible process. A budget becomes accounting and a roadmap becomes a product with consumed effort. The past and the future are treated with the same fundamental understanding.

Third of all, budgeting and roadmap processes are reputed as non-intrusive. They do not supplant negotiations. They are not the process of coordination. They are neutral, not any more required than optional when people negotiate and work together. The process can, however, provide more serenity and/or visibility.

So, now that the principles are down, perhaps we can get to what we are talking about. We are talking about putting in place an objective tool / process that will facilitate the rest, retract itself from operational considerations, and allow operational people to concentrate on the essential.
