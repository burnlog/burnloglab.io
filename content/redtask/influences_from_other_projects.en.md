---
title: "Influences from Other Projects"
date: 2017-04-10
source: "https://redtask.atlassian.net/wiki/spaces/CAP/pages/2293827/Influences+from+Different+Methodologies"
weight: 100
author: "Chris Mann <chris@mann.fr>"
---

This wiki page addresses how different methodologies address the art of efficient neutral iterative capacity-constrained software-release forecasting.

## How does Scrum of Scrums address this subject?

*   Content, wiki page or blog post to come from [Pierre Hervouet](https://redtask.atlassian.net/wiki/display/~phervouet).

## How does #NoEstimate address this subject?

*   Content, wiki page or blog post to come from [Christopher Mann](https://redtask.atlassian.net/wiki/display/~chris2fr).