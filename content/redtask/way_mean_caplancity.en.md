---
title: "Caplancity Ways and Means"
source: "https://redtask.atlassian.net/wiki/spaces/CAP/pages/2293835/Way+Mean+Capacity+Planning+Caplancity"
date: 2015-04-12
weight: 100
author: "Chris Mann <chris@mann.fr>"
---

_The Art of Efficient Agnostic Iterative Full-Information Capacity-Constrained Software-Release Roadmap Forecasting_

Dear Visitor,

This website is about confronting value (features in a release or "ways") and cost (work for the release or "means") on a same verifiable scale. Content is available on a publicly accessible and sign-onable [Confluence wiki](https://redtask.atlassian.net/wiki/pages/viewpage.action?pageId=2293762).

A subject of particular interest to me is mastery of co-variant impacts on credible interdependent rolling roadmaps with as little overhead as efficient. This can be important when calibrating vision across different heterogeneous ways and means with anticipatable interdependencies over longer time horizons than can be managed in compartmentalized approaches. It can be applied to contexts of multiple Scrum teams, or even just one scrum team across multiple sprints.

I am coming from experience with capacity planning in Prince 2 and Agile contexts, with different tools from spreadsheets to Clarity, and with writing an open-source plugin [Redmine_Taskjuggler](https://github.com/chris2fr/redmine_taskjuggler)for decentralized forecasting. I have seen the benefits of iterative capacity forecasting independant from negotiation in IT departments from dozens to hundreds of participants. It helps everyone stay on the same page.

Sincerely,

Christopher Mann
[christopher@mann.fr](mailto:christopher@mann.fr)
+33 7 81 811 811