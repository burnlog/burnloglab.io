---
title: "Meeting Notes"
source: "https://redtask.atlassian.net/wiki/spaces/CAP/pages/2293783/Meeting+notes"
date: 2017-07-05
weight: 100
author: "Chris Mann <chris@mann.fr>"
--- 

## Incomplete tasks from meetings

*   Regarder no-estimate [Christopher Mann](https://redtask.atlassian.net/wiki/display/~chris2fr) [2015-04-07 Meeting notes](https://redtask.atlassian.net/wiki/display/CAP/2015-04-07+Meeting+notes?focusedTaskId=2)
*   Lancer le Storymap à l'Openspace [Christopher Mann](https://redtask.atlassian.net/wiki/display/~chris2fr) [2015-04-07 Meeting notes](https://redtask.atlassian.net/wiki/display/CAP/2015-04-07+Meeting+notes?focusedTaskId=5) 
*   Regarder comment fait Scrum of Scrum [Pierre Hervouet](https://redtask.atlassian.net/wiki/display/~phervouet) [2015-04-07 Meeting notes](https://redtask.atlassian.net/wiki/display/CAP/2015-04-07+Meeting+notes?focusedTaskId=1) 

## All meeting notes

[2015-04-07 Meeting notes](/wiki/display/CAP/2015-04-07+Meeting+notes) [Christopher Mann](/wiki/display/~chris2fr) Jul 05, 2017