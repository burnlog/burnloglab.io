---
title: "Offre pour PME / PMI"
date: 2017-04-07
saurce: "https://redtask.atlassian.net/wiki/spaces/CAP/pages/2293774/Proposition+de+d+marche+de+planification+en+charges+pour+quipes+de+taille+mod+ste"
weight: 100
author: "Chris Mann <chris@mann.fr>"
---

Proposition de démarche de planification en charges pour équipes de taille modèste

Entreprises cibles :

*   Entreprises ayant un portefeuille de projets informatiques à gérer,
*   Toute entreprise gérant un portefeuille de projets avec ressources humaines et matérielles, avec sous-traitants et clients,
*   Editeurs informatiques,
*   Entreprises de réalisation de dispositifs sur mesure.

Nature de l’offre :

*   Apport d’un système à information complète sur des ressources et d’organigramme des tâches de portefeuille de projets,
*   Le niveau de modélisation dépend du niveau d’informations à disposition dans ce portefeuille,
*   L’offre de service ne nécessite pas, a priori, une collecte d’informations supplémentaires.

Mise en œuvre de l’offre :

*   1ère phase : prototypage (une semaine),
*   Puis implémentation dans la durée : ex : 1 jour/semaine pendant plusieurs mois.

Avantages de ce service :

*   Maîtrise des données : Ex : si une tâche est décalée, on sait mesurer le décalage avec les vacances des personnes concernées.
*   Meilleure maîtrise des délais : Ex : mise en évidence d’une tâche retardée pendant les vacances d’une ressource.
*   Maîtrise des impacts inter-projets : Ex : impact sur projet B quand projet A retardé.
*   Maîtrise de la valeur : Ex : on peut enregistrer les coûts des ressources et les revenus des projets, côte à côte. Contrôle de gestion constructif.
*   Tenue à jour, à intervalles réguliers, avec régularité, des comptes rendus d’activités.
*   Pré-remplir des CR d’activités.
*   Apporter un effet miroir au management d’équipe.Le modèle prend la logique et l’organisation de l’entreprise en entrées.
*   Modéliser les engagements avec les sous-traitants.
*   Donner aux chefs de projets la tenue des engagements par les managers d’équipe.

En conclusion, ce service permet :

*   de rationaliser la production à partir d’une même nomenclature entre le fournisseur et le client qu’au sein de chaque entité,
*   d’assurer aux employés et sous-traitants une sérénité de travail régulier qui va être demandé,
*   de donner des dates qui sont validées mathématiquement à partir d’hypothèses structurantes,
*   de renforcer la confiance dans les dates annoncées aux clients finaux,
*   d’être compatible avec l’état de l’art dans l’organisation de projets informatiques (SCRUM/AGILE, CMMI, Prince2/PMI, etc.)