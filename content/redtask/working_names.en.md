---
title: "Working Names"
source: "https://redtask.atlassian.net/wiki/spaces/CAP/pages/2293818/Working+Names"
date: 2015-04-10
weight: 100
author: "Chris Mann <chris@mann.fr>"
---


Here are the different names under consideration :

*   Caplancity  
    [www.caplancity.com](http://www.caplancity.com)
*   Way Mean >=!  
    [www.waymean.com](http://www.waymean.com)
*   Act Budget or Act Get Do  
    [www.actbudget.com
    www.actgetdo.com ](http://www.actbudget.com)

## Discussion for and against Caplancity

Caplancity is about putting planning in capacity. The main idea is to capitalize the remaining work provisions by operational participants. By capacity, I think the idea is more about "potential" than work.

## Discussion for and against Way Mean >=!

Way Mean is a play between "Ways and Means" and the evil nature we attribute to financial arbitration. The added benefit is the Pareto symbolization, at least as good as not. This commercial name may portray a certain energy to it.

## Discussion for and against Act Budget or Act Get Do

This concept or marketing approach describes both the idea of "actual" and "budget" in their interrelationships, alludes to the notion of an "actual budget" (hence the importance of an iterative budgeting process), and suggests that a budget exists for action. It is almost like a living budget.

Act Get Do is supposed to reference the approach of getting the actual, then updating the budget, provision, or forecast, followed by an execution phase.