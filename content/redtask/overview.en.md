---
longtitle: "Burnlog Overview from Atlassian Confluence Wiki"
title: "Overview"
dote:
draft: false
weight: 20
author: "Chris Mann <chris@mann.fr>"
toc: true
---

This is a topical wiki on the art of efficient agnostic iterative full-information capacity-constrained software-release roadmap forecasting. 

Ambitions of efficient neutral iterative full-information capacity-constrained software-release forecasts are potentially to :

*   demonstrate reasoning on the potential of teams to produce software under time and resource constraints;
*   modelize and measure covariant impacts of interrelated subjects;
*   incorporate systematically additional information;
*   establish a unique backlog to changelog around common user-stories;
*   reset systematically the backlog estimations at each iteration; 
*   and a couple of other things.

Software release forecasting is handled in different ways in [different contexts](https://redtask.atlassian.net/wiki/spaces/CAP/pages/2293827/Influences+from+Different+Methodologies) such as Scrum of Scrums, No Estimate and others. 

This effort is born from experience with many tools, and an open-source Redmine plugin redmine_taskjuggler. In one word, capacity planning is magic. It is a tool that, in the right hands, used in a good way, can bring a lot of good to development teams and those who foot the bill. More on this at [About](https://redtask.atlassian.net/wiki/spaces/CAP/pages/2293769/About)

Here are some random external links to get this page started:

[BurnLog.com](http://www.burnlog.com)  
The Home / Portal site with a redmine_taskjuggler instance

[Caplancity Jira](https://redtask.atlassian.net/projects/CAP)  
This is the JIRA instance for Caplancity hosted as on Open-Source application by Atlassian (Thank you)

[Redmine_Taskjugger](https://github.com/chris2fr/redmine_taskjuggler)
This is a plugin for Redmine that exports task information to Taskjuggler so that Taskjuggler can compute the tasks and send the results back to Redmine (dates).

[Taskjuggler](http://www.taskjuggler.org)  
This is a command-line / declarative text-driven planning and time-tracking tool. Fantastic software. Includes a burndown chart. Used for Fedora. Mainly for geeks.

[Clarity](http://www.ca.com/us/intellicenter/ca-ppm.aspx)  
This is a professional planning tool. The resolving tool is a derivative of Openworkbench. (What do they mean by "business agility?")

[PSNext](http://www.sciforma.com/)  
I am not sure about the scheduling capacities of this tool.

[Openworkbench](http://sourceforge.net/projects/openworkbench/)  
This is a single-user planning and time-tracking tool for a single user on a computer. Good software, opensource, on Windows only and not really maintained. (Is the source code still available.)


Some discussions have crystallized around idea of using time or of other more creative units to represent work than man-days. [Calibration](#) inevitably boils down to the subject of time-effort. Perhaps the fear that many have has something to do with strategic behaviors that confound projection and negotiation, if not with a simple basic human incapacity to evaluate time. Here is a potential page to discuss to [HumanTime](#) or not to [HumanTime](#) if that is your question. Some suggest [MonteCarlo](#) approaches, taking into account a spread of estimations, as an alternative to time as a negotiation point.

</div>