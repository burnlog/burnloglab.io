---
title: "Service Offering for SMBs"
date: 2017-04-07
saurce: "https://redtask.atlassian.net/wiki/spaces/CAP/pages/2293774/Proposition+de+d+marche+de+planification+en+charges+pour+quipes+de+taille+mod+ste"
weight: 100
author: "Chris Mann <chris@mann.fr>"
---

(Disclamer: this is a translated French-language text under proofreading.)

*Proposed capacity-planning approach for teams*

Target companies:

* Companies with a portfolio of IT projects to manage,
* Any company managing a portfolio of projects with human and material resources, with subcontractors and clients,
* Computer editors,
* Companies of realization of devices made to measure.

Nature of the offer:

* Provision of a complete information system on resources and organizational chart of project portfolio tasks,
* The level of modeling depends on the level of information available in this portfolio,
* The offer of service does not require, a priori, a collection of additional information.

Implementation of the offer:

* 1st phase: prototyping (one week),
* Then implementation over time: eg 1 day / week for several months.

Benefits of this service:

* Mastery of data: Ex: if a task is shifted, we know how to measure the shift with the holidays of the people concerned.
* Better control of deadlines: Ex: highlighting a delayed task during the holidays of a resource.
* Control of inter-project impacts: Ex: impact on project B when project has delayed.
* Value Mastery: Ex: Resource costs and project revenues can be recorded side by side. Constructive management control.
* Keeping regular updates of activity reports at regular intervals.
* Pre-fill Activity CRs.
* Bring a mirror effect to team management. The model takes the logic and organization of the company into entries.
* Model the commitments with the subcontractors.
* Give the project managers the commitment of the team managers.

In conclusion, this service allows:

* to rationalize the production from the same nomenclature between the supplier and the customer as within each entity,
* to ensure to employees and subcontractors a serenity of regular work that will be requested,
* to give dates that are validated mathematically from structuring hypotheses,
* strengthen confidence in the dates announced to end customers,
* to be compatible with the state of the art in the organization of IT projects (SCRUM / AGILE, CMMI, Prince2 / PMI, etc.)