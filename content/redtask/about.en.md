---
title: "About"
date: 2015-04-07
source: "https://redtask.atlassian.net/wiki/spaces/CAP/pages/2293769/About"
weight: 30
author: "Chris Mann <chris@mann.fr>"
---

Capacity Planning for Roadmap Credability is not for the weak-of-heart. This is a full-information system. That is like accounting (time accounting actually). The goal is to measure constrained covariant impacts. That is why you need to model everything. One thing moves here, that means that everything will adjust.

Caplancity is a space for those who want to master the art of capacity planning. This means that you are a time accountant. Time accountancy does not mean micro-management, it means setting up a tautologous system that shows you what you mean. Time accountancy implies:

*   Tools
*   Modelisation
*   Cooperation and Interactions

It gives you and your organization a sounding board. Correctly used, it can only be a trust and transparency support. It is a periodic system where the efforts for units of tasks are continually readjusted with regards to reality. The effort available is where the engagement lies, and the deadline becomes a variable. It sounds scary, but in reality it is about giving the necessary space to perform as a professional.