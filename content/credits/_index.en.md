---
title: "Credits"
hidden: true
date: 2017-11-21
weight: 1000
---

BurnLog is possible with the contributions of the following contributors.

**[Caplancity / Chris Mann](http://www.caplancity.com)** <chris@caplancity.com> for implementing the solutions.

Alexey Lukomskiy <lucomsky@gmail.com> for individual contributions.

**[Atlassian](http://www.atlassian.com)** for providing a test cloud server and tools for [*Redtask*](https://redtask.atlassian.net) as an Open-Source project.

**[Taskjuggler](http://www.taskjuggler.org)** used with many different clients with many different methods.

**[Redmine](http://www.redmine.org)** used in the *Wonderbox* installation.

**[GitHub](http://www.github.org)** and all GitHub contributors.

**[Clarity](https://www.ca.com/us/products/ca-project-portfolio-management.html)**  with contributions of Open Workbench and PMW for the *Pole Emploi* installation.

**[jellafornie](https://www.fiverr.com/jellafornie)** for the logo design.



