---
title: "Crédits"
date: 2017-11-21
weight: 1000
---

Burnlog est rendu possible grâce aux contributeurs suivants.

**[Caplancity / Chris Mann](http://www.caplancity.com)** <chris@caplancity.com> pour les implémentations.

Alexey Lukomskiy <lucomsky@gmail.com> pour contributions individuelles.

**[Atlassian](http://www.atlassian.com)** pour la mise à disposition de ses outils pour [*Redtask*](https://redtask.atlassian.net) en tant que projet open-source.

**[Taskjuggler](http://www.taskjuggler.org)** appliqué à des besoins variés.

**[Redmine](http://www.redmine.org)** utilsé par *Wonderbox* notamment.

**[GitHub](http://www.github.org)** et tous les contributeurs GitHub.

**[Clarity](https://www.ca.com/us/products/ca-project-portfolio-management.html)** avec contributions d'Open Workbench et PMW utilisés par Pôle Emploi.

**[jellafornie](https://www.fiverr.com/jellafornie)** pour le logo.



