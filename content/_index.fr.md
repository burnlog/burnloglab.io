---
title: "Burnlog : Planification en charges sympatique"
draft: false
weight: 10
date: 2017-11-20
---

[Burnlog](http://www.burnlog.com) de [Caplancity (Chris MANN)](http://www.caplancity.com) aide les organisations numériques mieux maîtriser leurs feuilles de route avec un package méthodes et outils. Ainsi les organisations pourraient harmoniser leurs négotiations en interne et en externe avec ce pont entre système de ticketing et de prévision.

*Attention: Les sources de ce site web sont en anglais. Il y a peu ou sinon aucune traduction. C'est mieux de voir [le site en anglais](/en/).*

![Burnlog Logo](/images/cmaps/burnlog_overview.png)

[Modifier cette page]({{% siteparam "editURL" %}}_index.fr.md)