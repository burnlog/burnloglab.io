---
title: "BurnLog – project Proposal"
hidden: true
draft: true
---

## Project title

BurnLog

## Proposed by

David Rhodes

## Summary of main issues and scope of the project

[Burnlog](http://www.burnlog.com) is a product and service package for better roadmap mastery by digital organizations (burn rate, backlog, etc.). Organizations may harmonize internal and external engagement negotiations with the help of software-tooled bridge between ticketing and capacity planning. This project is about the commercial launch of Burnlog by Caplancity.![Burnlog Logo](/images/logo/Burn-Log_logo_300.png)

## Objectives

*   Translate growth opportunity for a consulting company by launching its own product
*   Leverage on feedback from two experiments carried out with Pôle Emploi and Wonderbox
*   Build up alpha and beta users professional community
*   Refine value proposition
*   Study target market, perform competitive analysis
*   Define business model and licensing model
*   Develop partnerships
*   Market the product and service offering
*   Process financial critical mass constraints

## Sponsor (clients) sought for the project

![Caplancity Logo](/images/logo/caplancitylogoblack-128x64.png)[Caplancity](http://www.caplancity.com) (Chris Mann) is the client. Caplancity has obtained an existing partnership with Atlassian (Jira, etc.) for 2000 software licenses. Caplancity has moral engagements for partnership with Nemetschek (Atlassian Gold Partner, Tihomir Nikolov) for their software development (significant work done for a Jira plugin) and with Créatide (Eric Bouhereur) for his expertise in communication. Caplancity will seek as part of this project key users for implementation tests.

## Did the sponsor already give his/her agreement?

Yes

## Resources available for the project 

1 working open-source prototype,
2 successful end-client experiments (Pole Emploi, Wonderbox),
3 complementary partnerships

## Complementary skills sought for the project

Product Management  
Sales  
Marketing  
Legal

## Estimated workload

1 to 2 days per month

## Annexes

Burnlog-type software and method combinations have been applied by Chris Mann (Caplancity) in Pole Emploi (Clarity-based) and Wonderbox (publicly-available open-source product redmine_taskjuggler). Inspiration comes from Prince2 capacity planning, Agile #NoEstimates and Beyond Budgeting.![Burlog Overview](/images/cmaps/burnlog_overview.png)

