---
title: "Application Parameters"
description: "These are application-wide parameters."
date: 2014
weight: 100
---

The specification of the Taskjuggler path is in anticipation of running Taskjuggler on the server. That will be a future step that I have implemented elsewhere.

![Configure Application](/images/screenshots/configure-application.png)

There is also a way of putting together a RBS or Resource Breakdown Structure. Here is the screen.

![RBS](/images/screenshots/manage-teams.png)