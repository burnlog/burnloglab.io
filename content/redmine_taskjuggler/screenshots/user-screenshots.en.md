---
title: "User Screenshot"
date: 2014
weight: 60
---

The screenshots below show the extra fields associated with each user.

![Show User](/images/screenshots/user_show.png) 

![Edit User](/images/screenshots/user_edit.png)
