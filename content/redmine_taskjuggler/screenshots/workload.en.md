---
title: "Workload Weekly Timesheet"
description: "This is the weekly timesheet that users manage estimations and time worked."
date: 2014
weight: 100
---

This is the weekly timesheet that users manage estimations and time worked. I have to check the code. I think the interface may be in redmine_workload, but I thought I merged it into redmine_taskjuggler.

![Workload Weekly Timesheet Access](/images/screenshots/notice_workload.png)