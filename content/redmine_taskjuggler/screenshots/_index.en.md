---
title: "Screenshots"
date: 2014
description: "These are screenshots from the Redmine Taskjuggler plugin. The time consoldation screens and the global configuration screens are missing. There are the user, issue and project screens."
weight: 100
---

These are some screenshots from the plugin on the project, the issue and the user. There are also screens concerning facilitation of timesheet input for the users and teams as well as global configuration screens that are not listed here yet.

{{% children depth="3" description="true" %}}