---
title: "Issue Screenshot"
date: 2014
weight: 20
---
The screenshot below shows the extra fields associated with each issue.

![](/images/screenshots/issue_new.png)
