---
title: "Project Screenshot"
date: 2014
weight: 40
---

The screenshots below show the extra fields associated with the project definition.

![Project Overview](/images/screenshots/project_overview.png)

![Project Settings](/images/screenshots/project_settings.png)
