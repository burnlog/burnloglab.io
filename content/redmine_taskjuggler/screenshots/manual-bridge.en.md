---
title: "Manual Bridging Screenshots"
date: 2014
weight: 200
---

The screeshots below show the manual bridging process between the forecasting and ticketing softwares.

![Round-Trip Step 1](/images/screenshots/export_tjp_import_csv_step_2.png)

![Round-Trip Step 2](/images/screenshots/export_tjp_import_csv_step_4.png)

![Round-Trip Step 3](/images/screenshots/export_tjp_import_csv_step_5.png)

![Round-Trip Step 4](/images/screenshots/export_tjp_import_csv_step_7.png)

Below is a flow diagram of the use of the system.

![Round-Trip Overview](/images/cmaps/redmine_taskjuggler_workflow.png)