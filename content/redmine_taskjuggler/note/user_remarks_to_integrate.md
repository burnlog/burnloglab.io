---
title: "User Remarks"
weight: 100
author: "Chris Mann <chris@mann.fr>"
---

user_remarks_to_integrate

* Get TJ fields visible in views.
* Defaut all tj_activated fields to true
* Comment to Redmine that time is not an available field
