---
title: "HowTo Install Redmine_Taskjuggler"
language: en
weight: 30
date: 2017-11-20
---

First, install Redmine.

Then follow the instructions for installing of any plungin into Redmine. The redmine_taskjuggler plugin is available here:

https://github.com/chris2fr/redmine_taskjuggler

Specically, here are the download links:

* https://github.com/chris2fr/redmine_taskjuggler/archive/master.zip
* https://github.com/chris2fr/redmine_taskjuggler/archive/master.tar.gz

Here is the clone line if you have a GitHub link

`git clone git@github.com:chris2fr/redmine_taskjuggler.git`

So, here is what you do.

1. `cd` into your Redmine directory. On my demo server that would be `cd /opt/redmine/redmine-3.4.3/plugins/`.  The README says "Put your Redmine plugins here." so we will do just that.
1. Put the plugin there
   * with either `git clone git@github.com:chris2fr/redmine_taskjuggler.git` to get a git copy that you can contribute back to
   * or `git clone https://github.com/chris2fr/redmine_taskjuggler.git` if https is your way to go
   * or `wget https://github.com/chris2fr/redmine_taskjuggler/archive/master.tar.gz` followed by `tar -xzf master.tar.gz` and a `mv master/redmine_taskjuggler .` or something like that.
1. Stop Redmin if running `systemctl stop nginx` for me. (I was so had by that one!)
1. Adjust permissions. In my case I did a `addgroup geek www-data`, `chmod 775 $(find /opt/redmine/redmine-3.4.3/ -type d)` `chmod 664 $(find /opt/redmine/redmine-3.4.3/ -type f)` `chgrp -R www-data /opt/redmine/redmine-3.4.3/` as root
1. Insall that plugin `cd /opt/redmine/redmine-3.4.3/` `bundle install` (probably overkill) and then `bundle exec rake redmine:plugins RAILS_ENV=production`
1. Restart the Redmine server (`systemctl start nginx`), but there probably is a way to not shut down all of nginx if you do not wish to do so.

![Plugin Installed!](/images/screenshots/plugin_installed.png)

In my case, the demo installation is in http://redmine.burnlog.com.

Taskjuggler is not installed on the server. The TJI export is not working yet. The workload link doesn't seem to work even though that is essential. There is not test data charged and I forget if I have test data. More work yet to come. 