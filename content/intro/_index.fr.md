---
title: "Introduction"
weight: 15
date: 2017
hidden: false
disabletoc: false
chapter: false
lang: fr
---

Burnlog permet aux organisations de confier aux équipes opérationnelles des passerelles entre les systèmes de billetterie et les logiciels de prévision. La prévision récurrente est un outil adaptatif qui permet aux participants de communiquer sur les réalités sous-jacentes plutôt que sur les résultats finaux. Une partie de Burnlog est l'édition de logiciels, espérons-la open-source. Une autre partie aide les équipes interconnectées à trouver le bon niveau de décomposition du travail, de telle sorte que chaqu'item représente un engagement significatif entre les équipes. En confiant nos engagements à une norme objective, nous sommes mieux en mesure de fournir de l'espace pour le travail à effectuer.

## Partenariats

[Caplancity (Chris Mann)](http://www.caplancity.com) a obtenu un partenariat auprès d'Atlassian (Jira, etc.) licences de logiciels.

Caplancity a des engagements moraux pour un partenariat avec [Nemetschek](https://www.nemetschek.com) (Partenaire Or Atlassian, Tihomir Nikolov) pour leur développement logiciel (travail important réalisé pour un plugin Jira) et avec Créatide (Eric Bouhereur) pour son expertise en communication.

Caplancity cherchera dans le cadre de ce projet des utilisateurs clés pour les tests de mise en œuvre.

## Sucess Stories

Des combinaisons de logiciels et de méthodes de type Burnlog ont été appliquées par Chris Mann (Caplancity) dans Pole Emploi (basé sur Clarity) et Wonderbox (produit open-source publiquement disponible redmine_taskjuggler). L'inspiration vient de la planification de la capacité de Prince2, Agile #NoEstimates et Beyond Budgétisation.

## En travaux

Si vous regardez attentivement, il devrait y avoir une bonne quantité de contenu sur ce site. L'effort actuel ici est de consolider les friandises produites au cours des années.