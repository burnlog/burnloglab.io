---
title: "Introduction"
weight: 15
date: 2017
hidden: false
disabletoc: false
chapter: false
lang: en
---

Burnlog allows organizations to electively empower operational teams by proposing bridges between ticketing systems and forecasting software. Recurrent forecasting is an adaptive tool that allows for participants to communicate on underlying realities rather than on end results. Part of Burnlog is editing software, hopefully open-source. Another part is helping interconnected teams find the right level of work decomposition such that each element represents a meaningful engagement between teams. By entrusting our engagements to an objective standard, we are better able to provide space for the work at hand.

## Partnerships

[Caplancity (Chris Mann)](http://www.caplancity.com) has obtained an existing partnership with Atlassian (Jira, etc.) for 2000 software licenses.

Caplancity has moral engagements for partnership with [Nemetschek](https://www.nemetschek.com)  (Atlassian Gold Partner, Tihomir Nikolov) for their software development (significant work done for a Jira plugin) and with Créatide (Eric Bouhereur) for his expertise in communication.

Caplancity is now seeking key users.

## Sucess Stories

Burnlog-type software and method combinations have been applied by Chris Mann (Caplancity) in Pole Emploi (Clarity-based) and Wonderbox (publicly-available open-source product redmine_taskjuggler). Inspiration comes from Prince2 capacity planning, Agile #NoEstimates and Beyond Budgeting.

## Disclaimers

If you look carefully, there actually should be a fair amount of content on this website. The current effort here is to consolidate the tidbits produced over the years.

