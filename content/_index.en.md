---
title: "Burnlog : Capacity planning automation"
draft: false
weight: 10
date: 2017-11-20
---

[Burnlog](http://www.burnlog.com) by [Caplancity (Chris Mann)](http://www.caplancity.com) is a product and service package for digital organizations with regard to their better roadmap mastery (burn rate, backlog, etc.). Organizations may harmonize internal and external engagement negotiations with the help of a software-tooled bridge between ticketing and capacity planning.

[![Burnlog Logo](/images/cmaps/burnlog_overview.png)](./intro/)

[Edit This Page]({{% siteparam "editURL" %}}_index.en.md)