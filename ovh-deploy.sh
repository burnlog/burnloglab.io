#!/bin/sh
USER=geek
HOST=37.59.114.125
DIR=/var/www/html/burnlog.com
# might sometimes be empty!

hugo && rsync -avz --delete public/ ${USER}@${HOST}:${DIR}

exit 0
